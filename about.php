<?php 
    // -------- Header -------- //
    include "app/include/header.php";
    // -------- Header -------- //
?>

<!-- MAIN START-->
<div class="container mb-3">
        <div class="content row">

        <!-- MAIN_CONTENT START -->
            <div class="main-content pb-1 col-md-9 col-12 rounded-2">
                <h2>О проекте</h2>

                <p>
                    Gaming blog — коллектив авторов, который (вопреки своему названию) уже десять лет рассказывает всем желающим о видеоиграх. 
                    «Gaming blog» — это YouTube-канал с ежедневными видео, КЕК-канал с идиотскими нарезками, 
                    статьи, новости, аналитика...
                </p>

                <p>
                    Прямо сейчас вы зашли на нашу хаб-локацию: GamingBlog.com — платформа для нашего и вашего контента! 
                    Со своей стороны мы можем предложить:
                </p>

                <ul>
                    <li>Читать здесь статьи.</li>
                    <li>Общаться в комментариях (в том числе с нами).</li>
                    <li>Создавать блоги</li>
                </ul>
                
                <p>
                    Мы тут недавно сделали редизайн, поэтому сайт в активной разработке. 
                    Если у вас есть идеи, предложения или вы нашли ужасающий баг — шлите это нам через кнопку «Обратная связь» слева!
                </p>
            </div>
        <!-- MAIN_CONTENT END -->

        
        <!-- SIDEBAR_CONTENT START -->
            <div class="sidebar_hack sidebar col-md-3 col-12">
                <div class="sidebar_back rounded-2">
                    <h3>Помощь</h3>

                    <div class="section topics">
                        <ul>
                            <li>
                                <a href="<?= BASE_URL . "faq.php"?>">Вопросы</a>
                                <p></p>
                            </li>
                            <li class="active">
                                <a href="<?= BASE_URL . "about.php"?>">О проекте</a>
                                <p></p>
                            </li>
                            <li>
                                <a href="<?= BASE_URL . "contact.php"?>">Обратная свзяь</a>
                                <p></p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        <!-- SIDEBAR_CONTENT END -->
        </div>
    </div>
<!-- MAIN END-->




<?php 
    // -------- Fotter -------- //
    include "app/include/footer.php";
    // -------- Fotter -------- //
?>