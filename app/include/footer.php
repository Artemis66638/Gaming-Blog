        <footer class="footer">
            <div class="footer-content container">
                <div class="row">
                    <div class="footer-section about col-md-5 col-12">
                        <h3 class="logo-text mb-3">Gaming blog</h3>
                        <p>
                            Gaming blog - это блог сделаннный с целью информирования, развитию и продвижения игровой индустрии)).
                        </p>
                        <div class="contact">
                            <span><i class="fas fa-phone"></i> 123-456-789</span>
                            <span><i class="fas fa-envelope"></i> info@gamingblog.com</span>
                        </div>
                        <div class="socials mt-2">
                            <a href="https://www.instagram.com/"><i class="fab fa-instagram"></i></a>
                            <a href="https://twitter.com/"><i class="fab fa-twitter"></i></a>
                            <a href="https://www.youtube.com/"><i class="fab fa-youtube"></i></a>
                            <a href="https://vk.com/"><i class="fa-brands fa-vk"></i></a>
                            <a href="https://store.steampowered.com/"><i class="fa-brands fa-steam"></i></a>
                        </div>
                    </div>
                    <div class="footer-section links col-md-3 col-12"></div>
                    <div class="footer-section links col-md-4 col-12">
                        <h3 class="mb-3">Бысбтрые ссылки</h3>
                        <ul>
                            <li>
                                <a href="<?= BASE_URL?>">Главная</a>
                                <p></p>
                            </li>
                            <li>
                                <a href="<?= BASE_URL . "help.php"?>">Помощь</a>
                                <p></p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="footer-bottom container-fluid">
                &copy; gamingblog.com | Designed by Artem
            </div>
        </footer>
    </main>
    <!------------------------------------------------------ ICONS ------------------------------------------------------>
    <script src="https://kit.fontawesome.com/ea093bb07b.js" crossorigin="anonymous"></script>
    <!------------------------------------------------------ ICONS ------------------------------------------------------>

    <!------------------------------------------------------ BOOTSTRAP ------------------------------------------------------>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
    <!------------------------------------------------------ BOOTSTRAP ------------------------------------------------------>
  </body>
</html>