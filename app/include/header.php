<?php
    include "app/controllers/path.php";
?>

<!doctype html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Game blog</title>

    <!------------------------------------------------------ BOOTSTRAP ------------------------------------------------------>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <!------------------------------------------------------ BOOTSTRAP ------------------------------------------------------>

    <!------------------------------------------------------ STULE_CSS ------------------------------------------------------>
    <link rel="stylesheet" href="assets/css/style.css">
    <!------------------------------------------------------ STULE_CSS ------------------------------------------------------>

    <!------------------------------------------------------ GOOGLE_FONTS ------------------------------------------------------>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Press+Start+2P&display=swap" rel="stylesheet">
    <!------------------------------------------------------ GOOGLE_FONTS ------------------------------------------------------>
  </head>
  <body>
    <main>
        <header class="container-fluid">
            <div class="container">
                <div class="row">
                    <div class="col-4">
                        <h1>
                            <a href="<?= BASE_URL?>">Gaming Blog</a>
                        </h1>
                    </div>
                    <nav class="col-8">
                        <ul>
                            <li><a href="<?= BASE_URL?>">Главная</a> </li>
                            <li><a href="faq.php">Помощь</a> </li>
                            <li>
                                <a href="<?= BASE_URL . "log.php"?>"><i class="fa fa-user"></i> Войти</a>
                                <ul>
                                    <li><a href="<?= BASE_URL . "reg.php"?>" class="rounded-2">Регистрация</a> </li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </header>