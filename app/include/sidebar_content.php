<div class="sidebar_hack sidebar col-md-3 col-12">
    <div class="sidebar_back rounded-2">

        <div class="section search">
            <h3>Поиск</h3>
            <form action="search.php" method="post">
                <input type="text" name="search-term" class="text-input" placeholder="Поиск">
            </form>
        </div>

        <div class="section topics">
            <h3>Категории</h3>
            <ul>
                <li>
                    <a href="<?= BASE_URL . "category.php"?>">Игры</a>
                    <p></p>
                </li>
                <li>
                    <a href="<?= BASE_URL . "category.php"?>">Обзор</a>
                    <p></p>
                </li>
                <li>
                    <a href="<?= BASE_URL . "category.php"?>">Гайды</a>
                    <p></p>
                </li>
            </ul>
        </div>
    </div>
</div>