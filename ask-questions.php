<?php 
    // -------- Header -------- //
    include "app/include/header.php";
    // -------- Header -------- //
?>

<!-- MAIN START-->
<div class="container mb-3">
        <div class="content row">

        <!-- MAIN_CONTENT START -->
            <div class="main-content pb-1 col-md-9 col-12 rounded-2">
                <h2>Задать вопрос</h2>

                <div class="contact-form">
                    <form action="contact.php" method="post">
                        <input type="text" name="layout" class="contact-input text-input" id="layout" placeholder="Заголовок сообщения">
                        <textarea rows="4" name="message" class="text-input contact-input" id="message" placeholder="Ваше сообщение"></textarea>
                        <div class="block_btn pt-2">
                            <button type="submit" class="btn btn-big contact-btn p-3">
                                <i class="fas fa-envelope"></i> Отправить
                            </button>
                            <div  class="btn btn-big contact-btn p-3" id="btn_clear">
                                <i class="fa-solid fa-trash"></i> Очистить
                            </div>
                        </div>
                    </form>
                </div>
              
            </div>
        <!-- MAIN_CONTENT END -->

        <!-- SIDEBAR_CONTENT START -->
            <div class="sidebar_hack sidebar col-md-3 col-12">
                <div class="sidebar_back rounded-2">
                    <h3>Помощь</h3>

                    <div class="section topics">
                        <ul>
                            <li>
                                <a href="<?= BASE_URL . "faq.php"?>">Вопросы</a>
                                <p></p>
                            </li>
                            <li>
                                <a href="<?= BASE_URL . "about.php"?>">О проекте</a>
                                <p></p>
                            </li>
                            <li>
                                <a href="<?= BASE_URL . "contact.php"?>">Обратная свзяь</a>
                                <p></p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        <!-- SIDEBAR_CONTENT END -->
        </div>
    </div>
<!-- MAIN END-->

    <script src="assets/js/clear.js"></script>
<?php 
    // -------- Fotter -------- //
    include "app/include/footer.php";
    // -------- Fotter -------- //
?>