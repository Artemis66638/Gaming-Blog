<?php 
    // -------- Header -------- //
    include "app/include/header.php";
    // -------- Header -------- //
?>

<link rel="stylesheet" href="assets/css/questions.css">

<!-- MAIN START-->
<div class="container mb-3">
        <div class="content row">
    
        <!-- MAIN_CONTENT START -->
            <?php include("app/include/questions.php"); ?>
        <!-- MAIN_CONTENT END -->

        
        <!-- SIDEBAR_CONTENT START -->
            <div class="sidebar_hack sidebar col-md-3 col-12">
                <div class="sidebar_back rounded-2">
                    <h3>Помощь</h3>

                    <div class="section topics">
                        <ul>
                            <li class="active">
                                <a href="<?= BASE_URL . "faq.php"?>">Вопросы</a>
                                <p></p>
                            </li>
                            <li>
                                <a href="<?= BASE_URL . "about.php"?>">О проекте</a>
                                <p></p>
                            </li>
                            <li>
                                <a href="<?= BASE_URL . "contact.php"?>">Обратная свзяь</a>
                                <p></p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        <!-- SIDEBAR_CONTENT END -->
        </div>
    </div>
<!-- MAIN END-->




<?php 
    // -------- Fotter -------- //
    include "app/include/footer.php";
    // -------- Fotter -------- //
?>