<?php 
    // -------- Header -------- //
    include "app/include/header.php";
    // -------- Header -------- //
?>

<!-- MAIN START-->
    <div class="container mb-3">
        <div class="content row">

        <!-- MAIN_CONTENT START -->
            <div class="main-content col-md-9 col-12 rounded-2">
                <h2>Результат поиска</h2>

                <div class="all-post">
                    <div class="post row">
                        <div class="img col-12 col-md-4">
                            <img src="https://dummyimage.com/1200x700/6e70c2/ffffff" class="img-thumbnail">
                        </div>
                        <div class="post_text col-12 col-md-8">
                            <h3> <a href="<?= BASE_URL . "single_page.php"?>">Обзор игры</a> </h3>
                            <div class="atribut">
                                <span><i class="far fa-user"></i>Artemda</span>
                                <span><i class="far fa-calendar"></i>11.20.2002</span>
                                <span><i class="far fa-calendar"></i>Категория - игры</span>
                            </div>
                            
                            <p class="preview-text">Далеко-далеко за словесными горами в стране гласных и согласных живут, рыбные тексты.
                            </p>
                        </div>
                    </div>

                    <div class="post row">
                        <div class="img col-12 col-md-4">
                            <img src="https://dummyimage.com/1200x700/6e70c2/ffffff" class="img-thumbnail">
                        </div>
                        <div class="post_text col-12 col-md-8">
                            <h3> <a href="<?= BASE_URL . "single_page.php"?>">Обзор игры</a> </h3>
                            <div class="atribut">
                                <span><i class="far fa-user"></i>Artemda</span>
                                <span><i class="far fa-calendar"></i>11.20.2002</span>
                                <span><i class="far fa-calendar"></i>Категория - игры</span>
                            </div>
                            
                            <p class="preview-text">Далеко-далеко за словесными горами в стране гласных и согласных живут, рыбные тексты.
                            </p>
                        </div>
                    </div>

                    <div class="post row">
                        <div class="img col-12 col-md-4">
                            <img src="https://dummyimage.com/1200x700/6e70c2/ffffff" class="img-thumbnail">
                        </div>
                        <div class="post_text col-12 col-md-8">
                            <h3> <a href="<?= BASE_URL . "single_page.php"?>">Обзор игры</a> </h3>
                            <div class="atribut">
                                <span><i class="far fa-user"></i>Artemda</span>
                                <span><i class="far fa-calendar"></i>11.20.2002</span>
                                <span><i class="far fa-calendar"></i>Категория - игры</span>
                            </div>
                            
                            <p class="preview-text">Далеко-далеко за словесными горами в стране гласных и согласных живут, рыбные тексты.
                            </p>
                        </div>
                    </div>

                    <div class="post row">
                        <div class="img col-12 col-md-4">
                            <img src="https://dummyimage.com/1200x700/6e70c2/ffffff" class="img-thumbnail">
                        </div>
                        <div class="post_text col-12 col-md-8">
                            <h3> <a href="<?= BASE_URL . "single_page.php"?>">Обзор игры</a> </h3>
                            <div class="atribut">
                                <span><i class="far fa-user"></i>Artemda</span>
                                <span><i class="far fa-calendar"></i>11.20.2002</span>
                                <span><i class="far fa-calendar"></i>Категория - игры</span>
                            </div>
                            
                            <p class="preview-text">Далеко-далеко за словесными горами в стране гласных и согласных живут, рыбные тексты.
                            </p>
                        </div>
                    </div>
                </div>

            </div>
        <!-- MAIN_CONTENT END -->

        <!-- SIDEBAR_CONTENT START -->
            <?php include "app/include/sidebar_content.php"; ?>
        <!-- SIDEBAR_CONTENT END -->

        </div>
    </div>
<!-- MAIN END-->

<?php 
    // -------- Fotter -------- //
    include "app/include/footer.php";
    // -------- Fotter -------- //
?>