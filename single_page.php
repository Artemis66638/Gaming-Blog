<?php 
    // -------- Header -------- //
    include "app/include/header.php";
    // -------- Header -------- //
?>

<!-- CAROUSEL START-->
<div class="container">
    <div class="content row">
        <!-- Main Content -->
        <div class="main-content col-md-9 col-12 rounded-2">
            <h2>Обзор игры</h2>

            <div class="single_post row">
                <div class="img col-12">
                    <img src="https://dummyimage.com/1200x700/6e70c2/ffffff" alt="?>" class="img-thumbnail">
                </div>
                <div class="atribut">
                    <i class="far fa-user"></i><p>Artemda</p>
                    <i class="far fa-calendar"></i><p>11.20.2002</p>
                    <i class="far fa-calendar"></i><p>Категория - игры</p>
                </div>
                <div class="single_post_text col-12">
                    <p>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Ratione architecto molestias at veritatis assumenda placeat dolor maxime, 
                        accusamus explicabo consequuntur rem quasi voluptatibus fugit. Vel illum quia sed fugiat quo, nemo dolores ullam. 
                        Temporibus necessitatibus ea pariatur illo quaerat quas facilis error excepturi veniam. Eligendi, veniam nobis amet esse nemo, 
                        nulla fugiat ratione aspernatur, ex harum pariatur quia obcaecati labore. Praesentium ipsum laudantium eligendi accusantium natus 
                        culpa sapiente, tenetur officia omnis distinctio autem in, veniam repudiandae, repellat fuga recusandae vel aperiam maiores accusamus 
                        exercitationem dignissimos qui? Modi officia alias, dolorum sapiente incidunt voluptates, consectetur doloremque cum assumenda vel, nam illo.
                    </p>
                </div>
            </div>
        </div>

         <!-- SIDEBAR_CONTENT START -->
         <?php include "app/include/sidebar_content.php"; ?>
        <!-- SIDEBAR_CONTENT END -->

        <!-- COMMENTS_FORM START  --->
        <?php include("app/include/comments_form.php"); ?>
        <!-- COMMENTS_FORM END  --->

        <!-- COMMENTS START  --->
        <?php include("app/include/comments.php"); ?>
        <!-- COMMENTS END  --->

    </div>
</div>
<!-- MAIN END-->

<?php 
    // -------- Fotter -------- //
    include "app/include/footer.php";
    // -------- Fotter -------- //
?>