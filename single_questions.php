<?php 
    // -------- Header -------- //
    include "app/include/header.php";
    // -------- Header -------- //
?>

<link rel="stylesheet" href="assets/css/questions.css">

<!-- CAROUSEL START-->
<div class="container">
    <div class="content row">
        <!-- Main Content -->
        <div class="main-content single_questions col-md-9 col-12 rounded-2">
            <h2>Обзор игры</h2>

            <div class="single_questions_content row">
                <div class="atribut">
                    <span><i class="far fa-envelope"></i>awdawd</span>
                    <span><i class="far fa-calendar-check"></i>11.11.2002</span>
                </div>
                <div class="single_questions_text col-12">
                    <p>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Ratione architecto molestias at veritatis assumenda placeat dolor maxime, 
                        accusamus explicabo consequuntur rem quasi voluptatibus fugit. Vel illum quia sed fugiat quo, nemo dolores ullam. 
                        Temporibus necessitatibus ea pariatur illo quaerat quas facilis error excepturi veniam. Eligendi, veniam nobis amet esse nemo, 
                        nulla fugiat ratione aspernatur, ex harum pariatur quia obcaecati labore. Praesentium ipsum laudantium eligendi accusantium natus 
                        culpa sapiente, tenetur officia omnis distinctio autem in, veniam repudiandae, repellat fuga recusandae vel aperiam maiores accusamus 
                        exercitationem dignissimos qui? Modi officia alias, dolorum sapiente incidunt voluptates, consectetur doloremque cum assumenda vel, nam illo.
                    </p>
                </div>
            </div>
        </div>

        <!-- SIDEBAR_CONTENT START -->
            <div class="sidebar_hack sidebar col-md-3 col-12">
                <div class="sidebar_back rounded-2">
                    <h3>Помощь</h3>

                    <div class="section topics">
                        <ul>
                            <li>
                                <a href="<?= BASE_URL . "faq.php"?>">Вопросы</a>
                                <p></p>
                            </li>
                            <li>
                                <a href="<?= BASE_URL . "about.php"?>">О проекте</a>
                                <p></p>
                            </li>
                            <li>
                                <a href="<?= BASE_URL . "contact.php"?>">Обратная свзяь</a>
                                <p></p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        <!-- SIDEBAR_CONTENT END -->


        <!-- COMMENTS_FORM START  --->
        <?php include("app/include/comments_form.php"); ?>
        <!-- COMMENTS_FORM END  --->

        <!-- COMMENTS START  --->
        <?php include("app/include/comments.php"); ?>
        <!-- COMMENTS END  --->

    </div>
</div>
<!-- MAIN END-->

<?php 
    // -------- Fotter -------- //
    include "app/include/footer.php";
    // -------- Fotter -------- //
?>